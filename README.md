# Calculadora IMC

Projeto de Calculadora IMC utilizando HTML, CSS, Javascript, JQuery e Bootstrap.

## Como Utilizar

O projeto é bem simples, basta baixar o repositório e abrir o arquivo `index.html` através de um browser.

## Exemplo

Você pode ver projeto em http://codifika.com.br/bruno/calculadora-imc/

## Autores

* **Bruno Braz Silveira** - bbrazsilveira@gmail.com
