function calculate() {
    var weight = parseFloat(document.getElementById("weight").value);
    var height = parseFloat(document.getElementById("height").value);

    // Valida os campos
    if (isNaN(height) || height <= 0 || height >= 3) {
        showAlert("Altura inválida!", "Preencha sua altura (em metros) entre 0m e 3m.");
        return false;
    }
    if (isNaN(weight) || weight <= 0 || weight >= 300) {
        showAlert("Peso inválido!", "Preencha seu peso (em kilogramas) entre 0kg e 300kg.");
        return false;
    }

    // Calcula IMC
    var row;
    var rowStyle;
    var imc = weight / (height * height);
    var message = (imc >= 18.5 && imc < 24.9) ? "Parabéns! Seu IMC é de " : "Sinto muito, mas seu IMC é de ";
    message += imc.toFixed(1);

    // Personaliza a mensagem do resultado de acordo com a classificação do IMC
    if (imc < 18.5) {
        var varWeight = ((18.5 * (height * height)) - weight).toFixed(0);
        message += "kg/m², por isso você deveria ganhar no mínimo " + varWeight + "kg.";
        rowStyle = "table-warning";
        row = $('#row-light');
    } else if (imc >= 24.9) {
        var varWeight = (weight - (24.9 * (height * height))).toFixed(0);
        message += "kg/m², por isso você deveria emagrecer no mínimo " + varWeight + "kg.";

        if (imc <= 30) {
            rowStyle = "table-warning";
            row = $('#row-heavy');
        } else {
            rowStyle = "table-danger";
            row = $('#row-very-heavy');
        }
    } else {
        message += "kg/m² e você está dentro do peso adequado.";
        rowStyle = "table-success";
        row = $('#row-normal');
    }

    // Exibe o resultado
    showResult(message, row, rowStyle);
    return false;
}

function showAlert(title, message) {
    // Esconde a table IMC
    $('.container-table').hide();

    // Personaliza e exibe o diálogo
    $('#modalTitle').html(title);
    $('#modalMessage').html(message);
    $('#modal').modal('show');
}

function showResult(message, row, rowStyle) {
    // Personaliza e exibe a tabela IMC
    $('.container-table').show();
    $('#row-light').removeClass();
    $('#row-normal').removeClass();
    $('#row-heavy').removeClass();
    $('#row-very-heavy').removeClass();
    row.addClass(rowStyle);

    // Personalize e exibe o diálogo
    $('#modalTitle').html("Seu Resultado");
    $('#modalMessage').html(message);
    $('#modal').modal('show');
}